function myAlert(title, value){
    var alert = document.getElementById('message');
    alert.style.display = "block";
    alert.innerHTML = '<div class="alert"><div class="inner"><div class="alertTitle">' + title + '</div><div class="text">' + value + '</div></div><div class="button" id="alertButton">OK</div></div>';
    var buton = document.getElementById('alertButton');
    buton.addEventListener('click',function(){
        alert.innerHTML = "";
        alert.style.display = "none";
    });
}