var locationForm = [];
var destinations = [];
var currentFocus;
function autocomplete(inp) {
    
    inp.addEventListener("input", function(e) {
       const coppy = listOfItems;
        var a, b, i, val = this.value;
        if(val !=''){
          JSONData('https://geo.flotea.pl/v1/autocomplete?text='+val+'&mobile='+dev, returnElement);
          } 
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        
        for (var i = 0; i < coppy.length; i++) {
  
            var b = document.createElement("DIV");
            var bb = document.createElement("DIV");
            b.className = "list";
            bb.className = "listBox";
            b.innerHTML = "<img src='./img/pointer.png' alt='from' title='to' data-id='1'>";    
            bb.innerHTML += "<strong>" +coppy[i][0]+", "+coppy[i][2]+"</strong><br>";
            bb.innerHTML += coppy[i][1];
            bb.innerHTML += "<input type='hidden' data-id='"+i+"' value='" + coppy[i][0]+", "+coppy[i][2]+"'>";
            bb.addEventListener("click", function(e) {
                  
                  inp.value = this.getElementsByTagName("input")[0].value;
                  inp.style.color = "#1E1E23";
                  header.style.position="fixed";
                  locationForm[inp.attributes[5]['value']]=coppy[this.getElementsByTagName("input")[0]['attributes'][1]['value']];
                  destinations[inp.attributes[5]['value']] = coppy[this.getElementsByTagName("input")[0]['attributes'][1]['value']];
                  closeAllLists();
            });
            var c = a.appendChild(b);
            c.appendChild(bb);
          }
        
    });
    inp.addEventListener("keydown", function(e) {
        var currentFocus;
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          currentFocus++;
          addActive(x);
        } else if (e.keyCode == 38) { 
          currentFocus--;
          addActive(x);
        } else if (e.keyCode == 13) {
          e.preventDefault();
          if (currentFocus > -1) {
            if (x) x[currentFocus].click();
          }
        }
    });
  
    function addActive(x) {
      if (!x) return false;
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      x[currentFocus].classList.add("autocomplete-active");
    }
  
    function removeActive(x) {
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    
    function closeAllLists(elmnt) {
      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
  
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
    
  }
  function keyBoard(){
    header.style.position="absolute";
    from.parentNode.children[0].style.display = "none";
    
  }
  function clearFrom(){
    from.value="";
    from.style.border = "";
    keyBoard();
  }
  function clearTo(){
    to.value = "";
    to.style.border = "";
    keyBoard()
  }
  function clearTel(){
    input.value = "";
    input.style.border = "";
    keyBoard();
  }
